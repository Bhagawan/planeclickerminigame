package com.example.planeclicker.ui.view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.view.View
import com.example.planeclicker.data.stat.Assets
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.math.sin
import kotlin.random.Random

class PlaneClickerGameView(context: Context): View(context){

    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeAngle = 0.0f
    private var targetPlaneAngle = 0.0f
    private var planeAngleTimer = 20
    private var plane = Assets.planeBitmap?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private var clockwiseRotation = true
    private var rotationAngle = 0.0f

    private var planeDrop = false

    private var score = 0

    companion object {
        const val STATE_PAUSE = 0
        const val STATE_GAME = 1
    }

    private var currentState = STATE_GAME

    private var mInterface: GameInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            val pW = mWidth * 0.8f
            plane = Bitmap.createScaledBitmap(plane,
                pW.toInt(), (pW * plane.height / plane.width.toFloat()).toInt(), true)
        }
    }

    override fun onDraw(canvas: Canvas) {
        if(currentState == STATE_GAME) updatePlane()
        drawPlane(canvas)
    }

    //// Public

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    fun start() {
        if(currentState == STATE_PAUSE || planeDrop) {
            planeY = mHeight / 2.0f
            planeAngle = 0.0f
            targetPlaneAngle = 0.0f
            rotationAngle = 0.0f
            planeAngleTimer = 100
            planeDrop = false
            score = 0
            currentState = STATE_GAME
        }
    }

    fun push(left: Boolean) = pushPlane(left)

    //// Private

    private fun drawPlane(c: Canvas) {
        c.drawBitmap(getRotatedBitmap(plane, planeAngle), planeX  - plane.width / 2.0f, planeY - plane.height / 2.0f, Paint())
    }

    private fun updatePlane() {
        if(!planeDrop) {
            rotationAngle++
            if(rotationAngle >= 360) {
                rotationAngle = 0.0f
                clockwiseRotation = clockwiseRotation.not()
            }
            val v = rotationAngle * if(clockwiseRotation) 1 else -1
            val dX = if(rotationAngle < 180) rotationAngle * if(clockwiseRotation) 1 else -1
            else (180 - rotationAngle % 180) * if(clockwiseRotation) 1 else -1
            planeX = mWidth / 2.0f + dX
            planeY = (mHeight / 2.0f + sin(Math.toRadians(v.toDouble())) * 360 * 0.2f).toFloat()

            if(planeAngleTimer-- <= 0) {
                targetPlaneAngle += (Random.nextInt(100) * 0.3f).toInt() * if(Random.nextBoolean()) 1 else -1
                planeAngleTimer = 20
            }
            planeAngle += if(targetPlaneAngle > planeAngle) 0.2f else if(targetPlaneAngle < planeAngle) -0.2f else 0.0f
            if(planeAngle.absoluteValue >= 45) {
                planeDrop = true
                mInterface?.fail()
            }
            else mInterface?.updateScore(score++)
        } else {
            planeY++
            if(planeY >= mHeight) currentState = STATE_PAUSE
        }
    }

    private fun pushPlane(left: Boolean) {
        targetPlaneAngle = planeAngle + 10 * if(left) 1 else -1
    }

    private fun getRotatedBitmap(bitmap: Bitmap, angle: Float): Bitmap {
        val m  = Matrix()
        m.postRotate(angle, bitmap.width / 2.0f, bitmap.height / 2.0f)
        return Bitmap.createBitmap(bitmap, 0,0,bitmap.width, bitmap.height,m, true )
    }

    interface GameInterface {
        fun updateScore(score: Int)
        fun fail()
    }
}