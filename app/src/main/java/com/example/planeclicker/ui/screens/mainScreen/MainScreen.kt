package com.example.planeclicker.ui.screens.mainScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.planeclicker.data.stat.UrlBack
import com.example.planeclicker.ui.composables.FailPopup
import com.example.planeclicker.ui.composables.PushButton
import com.example.planeclicker.ui.theme.Gold
import com.example.planeclicker.ui.view.PlaneClickerGameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun MainScreen() {
    val viewModel = viewModel(MainScreenViewModel::class.java)
    val score by viewModel.score.collectAsState()
    var init  by rememberSaveable { mutableStateOf(true) }
    val failPopup by viewModel.failPopup.collectAsState()


    Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    Row(modifier = Modifier.fillMaxSize()) {
        BoxWithConstraints(modifier = Modifier
            .weight(1.0f, true)
            .fillMaxHeight()) {
            val w = maxWidth
            PushButton(modifier = Modifier
                .width(w)
                .align(Alignment.BottomCenter)
                .padding(20.dp), onClick = { viewModel.push(true) })
        }
        Box(modifier = Modifier
            .weight(3.0f, true)
            .fillMaxHeight()) {
            Text(score.toString(), fontSize = 20.sp, textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold, color = Gold,
                modifier = Modifier.align(Alignment.TopCenter))
            AndroidView(factory = { PlaneClickerGameView(it) },
                modifier = Modifier.fillMaxSize(),
                update = {view ->
                    if (init) {
                        view.setInterface(object: PlaneClickerGameView.GameInterface {
                            override fun updateScore(score: Int) {
                                viewModel.updateScore(score)
                            }

                            override fun fail() {
                                viewModel.fail()
                            }

                        })
                        viewModel.planePush.onEach {
                            if(it != null) view.push(it)
                        }.launchIn(viewModel.viewModelScope)
                        viewModel.restart.onEach {
                            if(it) view.start()
                        }.launchIn(viewModel.viewModelScope)
                        init = false
                    }
                })
        }
        PushButton(modifier = Modifier
            .weight(1.0f, true)
            .align(Alignment.Bottom)
            .padding(20.dp), onClick = { viewModel.push(false) })
    }

    if(failPopup) FailPopup(score, onRestart = viewModel::restart)

}