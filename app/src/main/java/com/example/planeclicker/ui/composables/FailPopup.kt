package com.example.planeclicker.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.planeclicker.R
import com.example.planeclicker.ui.theme.Orange
import com.example.planeclicker.ui.theme.Red
import com.example.planeclicker.ui.theme.Transparent_black
import com.example.planeclicker.ui.theme.Yellow_light

@Composable
fun FailPopup(score: Int = 0, onRestart: () -> Unit = {}) {
    AnimatedVisibility(visible = true,
        enter = fadeIn(tween(500))
    ) {
        Box(modifier = Modifier
            .fillMaxSize()
            .background(Transparent_black), contentAlignment = Alignment.Center) {
            Column(modifier = Modifier
                .fillMaxWidth(0.5f)
                .background(Yellow_light, RoundedCornerShape(10.dp))
                .border(width = 2f.dp, color = Orange, shape = RoundedCornerShape(10.dp))
                .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
                Text(stringResource(id = R.string.header_fail), fontSize = 30.sp, color = Red, textAlign = TextAlign.Center)
                Text(stringResource(id = R.string.record), fontSize = 15.sp, color = Color.White, textAlign = TextAlign.Center)
                Text(score.toString(), fontSize = 15.sp, color = Color.White, textAlign = TextAlign.Center)
                Image(painterResource(id = R.drawable.ic_restart),
                    contentDescription = null,
                    contentScale = ContentScale.FillWidth,
                    modifier = Modifier
                        .fillMaxWidth(0.15f)
                        .clickable(remember { MutableInteractionSource() }, null) { onRestart() }
                        .clipToBounds())
            }
        }
    }
}