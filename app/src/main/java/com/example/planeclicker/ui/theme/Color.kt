package com.example.planeclicker.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Transparent_black = Color(0x4D000000)
val Green_dark = Color(0xFF194912)
val Yellow_light = Color(0xFFE2CA83)
val Orange = Color(0xFFB9561E)
val Gold = Color(0xFFFFBF00)
val Red = Color(0xFFA31B1B)