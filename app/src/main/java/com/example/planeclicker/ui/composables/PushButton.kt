package com.example.planeclicker.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.planeclicker.R
import com.example.planeclicker.ui.theme.Green_dark
import com.example.planeclicker.ui.theme.Transparent_black

@Composable
fun PushButton(modifier: Modifier = Modifier, onClick: () -> Unit = {}) {
    Box(modifier = modifier
        .aspectRatio(1.0f )
        .background(Transparent_black, CircleShape)
        .border(width = 2.dp, color = Green_dark, CircleShape)
        .clip(CircleShape)
        .clickable(remember { MutableInteractionSource() }, LocalIndication.current) { onClick() }
        .padding(10.dp),
        contentAlignment = Alignment.Center) {
        Image(painter = painterResource(id = R.drawable.baseline_gps_fixed_24), contentDescription = null, contentScale = ContentScale.FillBounds, modifier = Modifier.padding(10.dp).fillMaxSize())
    }
}