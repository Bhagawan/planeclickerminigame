package com.example.planeclicker.ui.screens.mainScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainScreenViewModel:ViewModel() {
    private val _planePush = MutableSharedFlow<Boolean?>()
    val planePush = _planePush.asSharedFlow()

    private val _restart = MutableSharedFlow<Boolean>()
    val restart = _restart.asSharedFlow()

    private val _score = MutableStateFlow(0)
    val score = _score.asStateFlow()

    private val _failPopup = MutableStateFlow(false)
    val failPopup = _failPopup.asStateFlow()

    fun push(left: Boolean) {
        viewModelScope.launch {
            _planePush.emit(left)
        }
    }

    fun fail() {
        _failPopup.tryEmit(true)
    }

    fun restart() {
        viewModelScope.launch {
            _score.emit(0)
            _failPopup.emit(false)
            _restart.emit(true)
        }
    }

    fun updateScore(score: Int) {
        _score.tryEmit(score)
    }
}