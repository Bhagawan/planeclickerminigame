package com.example.planeclicker.data.stat

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import coil.ImageLoader
import coil.request.ErrorResult
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.example.planeclicker.data.AssetsState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

object Assets {
    private val downloadScope = CoroutineScope(EmptyCoroutineContext)

    var planeBitmap: Bitmap? = null

    private val _assetsState = MutableStateFlow(AssetsState.LOADING)
    val assetsState = _assetsState.asStateFlow()

    fun loadAssets(context: Context) {
        downloadBitmap(context, UrlAssetPlane,
            onSuccess =  {
                planeBitmap = it
                _assetsState.tryEmit(AssetsState.LOADED)
            },
            onError =  {
                _assetsState.tryEmit(AssetsState.ERROR)
            })
    }


    private fun downloadBitmap(context: Context,
                    url: String,
                    onSuccess: (bitmap: Bitmap) -> Unit,
                    onError: (error: Throwable) -> Unit) {
        var bitmap: Bitmap? = null
        val loadBitmap = downloadScope.launch(Dispatchers.IO) {
            val loader = ImageLoader(context)
            val request = ImageRequest.Builder(context)
                .data(url)
                .allowHardware(false)
                .build()
            val result = loader.execute(request)
            if (result is SuccessResult) {
                bitmap = (result.drawable as BitmapDrawable).bitmap
            } else if (result is ErrorResult) {
                cancel(result.throwable.localizedMessage ?: "ErrorResult", result.throwable)
            }
        }
        loadBitmap.invokeOnCompletion { throwable ->
            bitmap?.let {
                onSuccess(it)
            } ?: throwable?.let {
                onError(it)
            } ?: onError(Throwable("Undefined Error"))
        }
    }
}