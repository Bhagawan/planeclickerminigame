package com.example.planeclicker.data

import androidx.annotation.Keep

@Keep
data class PlaneClickerSplashResponse(val url : String)