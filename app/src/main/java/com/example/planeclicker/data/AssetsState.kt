package com.example.planeclicker.data

enum class AssetsState {
    LOADING, ERROR, LOADED
}