package com.example.planeclicker.util.api

import com.example.planeclicker.data.PlaneClickerSplashResponse
import com.example.planeclicker.data.stat.UrlSplash
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PlaneClickerServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<PlaneClickerSplashResponse>

    companion object {
        fun create() : PlaneClickerServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(PlaneClickerServerClient::class.java)
        }
    }
}