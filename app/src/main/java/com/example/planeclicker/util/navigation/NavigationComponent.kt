package com.example.planeclicker.util.navigation

import android.webkit.WebView
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.planeclicker.data.stat.AppData
import com.example.planeclicker.ui.screens.NetworkErrorScreen
import com.example.planeclicker.ui.screens.Screens
import com.example.planeclicker.ui.screens.SplashScreen
import com.example.planeclicker.ui.screens.WebViewScreen
import com.example.planeclicker.ui.screens.mainScreen.MainScreen
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: WebView, fixateScreen:  () -> Unit, reloadAssets: () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            BackHandler(true) { exitProcess(0) }
            SplashScreen()
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.canGoBack()) webView.goBack()
                else exitProcess(0)
            }
            WebViewScreen(webView, remember { AppData.url })
        }
        composable( Screens.MAIN_SCREEN.label ) {
            BackHandler(true) { exitProcess(0) }
            MainScreen()
        }
        composable( Screens.NETWORK_ERROR_SCREEN.label ) {
            BackHandler(true) { exitProcess(0) }
            NetworkErrorScreen(reloadAssets)
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen != Screens.WEB_VIEW) fixateScreen()
    navController.navigate(currentScreen.label) {
        launchSingleTop = true
    }
    navController.enableOnBackPressed(true)
}